import Vue from 'vue'
import App from './App.vue'
import router from './router'
import 'element-ui/lib/theme-chalk/index.css'
import { Table, TableColumn, Button, Pagination } from 'element-ui'
Vue.use(Table).use(TableColumn).use(Button).use(Pagination)
Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')

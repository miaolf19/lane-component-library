# Lane-Component-Library


## 安装
```
npm install lane-ui
```

## 全局导入
```
import LaneUI from 'lane-ui'
import 'lane-ui/dist/lane-ui.css'
Vue.use(LaneUI)
```

### tableConfig
|    参数 | 说明 | 类型 | 可选值 | 默认值 |
|  ----  | ----  |  ----  | :----:  |:----:|
| data  | 数据 | array  | -- |-- |
| headerBgColor  | 标题行背景色 | string  | -- |-- |
| rowAttr  | el-table其他未定义属性 | object  | -- |-- |
| rowMethod  | el-table其他未定义方法 | object  | -- |-- |
| [itemConfig](#itemConfig)  | 表格配置项 | object  | -- |-- |


#### <font color='orange' size=3> 提示 </font>
<font size=2 color='orange'>1. 原有ElementUI的其他属性与方法，可通过rowAttr和rowMethod传递使用，传递table对象示例：</font>
```
rowAttr: {border: true, stripe: true}
rowMethod: {'selection-change': this.handleSelectionChange}
```
<font size=2 color='orange'>2. pageConfig不可传递rowMethod参数</font>




<h3 id=itemConfig>tableConfig.itemConfig</h3>

|    参数 | 说明 | 类型 | 可选值 | 默认值 |
|  ----  | ----  |  ----  | :----:  |:----:|
| width | 宽度 | number  | -- | 55 |
| prop  | 列内容的字段名 | string  | -- | -- |
| label  | 显示的标题 | string  | -- | -- |
| filter  | 列内容过滤器，可输入数字```1```：列内容保留两位小数；```2```：列内容末尾去零。输入自定义函数```function(row,name) {}```，其中```row```为行数据，```name```为对应列的```prop```，```row[name]```为对应列数据 | number/function  | 1/2/自定义函数 | left |
| type  | 对应列的类型。如果设置了 ```selection``` 则显示多选框；如果设置了 ```index``` 则显示该行的索引（从 1 开始计算）。```注意：此属性若选择，则该列数据不显示，暂时不支持expand属性``` | string  | index,selection | -- |
| slot  | 是否开启插槽，[插槽使用方法](#slot) | boolean  | -- | false |
| rowAttr  | el-table-column其他未定义属性 | object  | -- |-- |
| rowMethod  | el-table-column其他未定义方法 | object  | -- |-- |


<h3 id=pageConfig>pageConfig</h3>

|    参数 | 说明 | 类型 | 可选值 | 默认值 |
|  ----  | ----  |  ----  | :----:  |:----:|
| pageNo | 当前页 | number  | -- | -- |
| total | 数据总条数 | number  | -- | -- |
| pageSize | 每页数据量 | number  | -- | -- |
| layout | 分页器组件 | string  | sizes,prev,pager,next,jumper,total,slot | total,prev,pager,next,jumper |
| show | 分页器是否显示 | boolean  | true,false | true |
| pageSizeArr | 每页显示个数选择器的选项设置 | number[]  | -- | [10,20,30,40,50,100] |


<h3 id=tableEvent>Table Events</h3>

|    事件名称 | 说明 | 回调参数 |
|  ----  | ----  |  ----  | 
| pageConfigChange | ```pageSize```或```pageNo```改变触发 | {pageNo,pageSize,total}  | 


<h4 id=slot>插槽使用方法</h4>

```vue
<!-- 使用template标签或者div其他标签均可，row为自定义接收数据名，slot的值为对应操作列的prop，否则不生效 -->
示例：
<template slot-scope="row" slot="salesPrice">
  <button @click="showData(row.data)">点击显示数据</button>
</template>
```



##### 解决问题
1. 解决pagesize变更时可能同时触发pageChange回调的问题
2. 解决el-table使用时，各列均设置固定宽度，导致table宽度达不到100%而出现留白的问题


##### 版本更新
1. 2024-01-19 0.1.0 初始版本
2. 2024-01-22 0.1.1 修复分页器默认不显示bug
3. 2024-01-24 0.1.2 修复table-column的index重复问题，修改组件继承方式，优化代码







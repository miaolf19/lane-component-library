export default {
  name: 'LaTable',
  props: ['tableConfig', 'pageConfig', 'rowTableConfig'],
  data () {
    return {
      page: {}
    }
  },
  filters: {},
  computed: {
    tableWidth () {
      return this.tableConfig.itemConfig.some((item, index) => {
        return !item.width
      })
        ? '100%'
        : this.tableConfig.itemConfig.reduce((pre, cur) => {
          return pre + +cur.width
        }, 1) + 'px'
    }
  },

  created () {
    this.page = Object.assign({}, this.pageConfig)
    console.log(this.tableConfig.rowMethod)
    console.log(this.$listeners)
  },
  methods: {
    formatChoose (row, name, process = 1) {
      // 1-保留两位小数 2-末尾去零 func自定义处理
      if (Object.prototype.toString.call(process) === '[object Function]') {
        return process(row, name)
      } else if (Object.prototype.toString.call(process) === '[object Number]') {
        switch (process) {
          case 1:
            return this.formatDecimal(row, name)
          case 2:
            return this.formatMoney(row, name)
          case 3:
            return row.choose === 1 ? '是' : '否'
          case 4:
            return row.choose === 1 ? '是' : '否'
          case 5:
            return row.choose === 1 ? '是' : '否'
        }
      } else {
        throw Error('itemConfig.filter.process参数类型错误')
      }
    },
    // 末尾去零
    formatMoney (row, name) {
      if (!row[name]) return (0).toFixed(2)
      return parseFloat(Number(row[name]))
    },
    // 保留两位小数
    formatDecimal (row, name, n = 2) {
      const num = row[name]
      if (!num) return (0).toFixed(2)
      return Number(num).toFixed(n)
    },
    // page变更
    pageChange () {
      console.log('page变更')
      this.$emit('pageConfigChange', this.page)
    },
    // pageSize变更
    pageSizeChange () {
      console.log('pageSize变更')
      // 解决pagesize变更时可能同时触发pageChange回调的问题
      const { pageNo, pageSize, total } = this.page
      if (pageNo * pageSize > total) return
      this.pageChange()
    },
    typeSelect (type) {
      return !type || type === 'expand'
    }
  }
}
